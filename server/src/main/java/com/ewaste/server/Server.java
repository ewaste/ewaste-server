package com.ewaste.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;


import com.ewaste.kernel.IKernel;

public class Server<InT,ResT> implements Runnable {

    private long startId = new Random().nextLong();
    private Thread listenThread;
    private ServerSocket socket;
    private ArrayList<Client> clients = new ArrayList<>();
    private KernelUtils kernelUtils;
    byte[] dexKernel;
    String kernelClassName;
    private ConcurrentLinkedQueue<InT> dataset;
    private ConcurrentLinkedQueue<ResT> results;
    private int datasetCounter;
    IServerCallbacks<InT, ResT> callbacks;
    int packCount;

    public Server(
            int port,
            String dxPath,
            IKernel kernel,
            ConcurrentLinkedQueue<InT> dataset,
            IServerCallbacks<InT, ResT> callbacks,
            int packCount
    ) throws IOException {

        socket = new ServerSocket(port);
        kernelUtils = new KernelUtils(dxPath);
        dexKernel = kernelUtils.convertToDex(kernel);
        kernelClassName = kernel.getClass().getName();
        listenThread = new Thread(this);
        listenThread.start();
        this.dataset = dataset;
        datasetCounter = dataset.size();
        results = new ConcurrentLinkedQueue<>();
        this.callbacks = callbacks;
        this.packCount = packCount;
    }

    List<InT> getNextData(int n) {
        if (n < 1) {
            U.err("Requested data count must be larger than 0.");
        }
        ArrayList<InT> data = new ArrayList<>();
        InT d;
        for (int i = 0; i < n; i++) {
            d = dataset.poll();
            if (d == null) {
                if (data.isEmpty()) {
                    return null;
                } else {
                    return data;
                }
            }
            data.add(d);
        }
        return data;
    }

    public ConcurrentLinkedQueue<ResT> getResults() {
        return results;
    }

    void returnData(List<InT> data) {
        dataset.addAll(data);
    }

    void appendResult(List<InT> inputs, List<ResT> results) {
        for(int i=0;i<inputs.size();i++) {
            callbacks.gatherResult(inputs.get(i), results.get(i));
        }
        datasetCounter -= inputs.size();
        if(0 == datasetCounter) {
            int n = 0;
            boolean done = true;
            while(n<10) {
                for(Client c: clients) {
                    if(!c.done) {
                        done = false;
                    }
                }
                if (done) {
                    break;
                }
                n++;
                try {
                    Thread.sleep(100);
                } catch (Exception e) {
                    U.err("Server failed to shutdown.");
                    break;
                }
            }
            Thread shutdownThread = new Thread(new ShutdownRunnable(this));
            shutdownThread.start();
        }
    }

    private boolean accept = true;

    public void stop() {
        accept = false;
        try {
            socket.close();
            listenThread.join(1000);
            listenThread.interrupt();
            int n = 0;
            boolean done = true;
            while(n<10) {
                for(Client c: clients) {
                    if(!c.done) {
                        done = false;
                    }
                }
                if (done) {
                    break;
                }
                n++;
                try {
                    Thread.sleep(100);
                } catch (Exception e) {
                    U.err("Server failed to shutdown.");
                    break;
                }
            }
        } catch (InterruptedException | IOException e) {
            U.err( e.getMessage() );
        }
    }

    public void run() {
        try {
            while (accept) {
                Socket s = socket.accept();
                if(s != null) {
                    Client client = new Client<InT, ResT>(s, ++startId, this);
                    clients.add(client);
                }
            }
            socket.close();
        } catch (IOException e) { }
    }

    public void waitFor() throws InterruptedException {
        listenThread.join();
    }
}

class ShutdownRunnable implements Runnable {
    private Server server;
    public ShutdownRunnable(Server server) {
        this.server = server;
    }
    @Override
    public void run() {
        server.stop();
        U.out("Server shut down.");
    }
}