package com.ewaste.server;

import com.ewaste.kernel.IKernel;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

class KernelUtils {
    private String dxPath;

    KernelUtils(String dexCompilerPath){
        this.dxPath = dexCompilerPath;
    }

    byte[] convertToDex(IKernel kernel) throws IOException {
        Class kernelClass = kernel.getClass();
        String kernelClassPath = kernelClass.getResource(kernelClass.getSimpleName() + ".class").getPath();
        File kernelClassDir = new File(kernelClass.getProtectionDomain().getCodeSource().getLocation().getPath());
        String dexPath = "/tmp/kernel.dex";
        String cmd = dxPath + " --dex --output "  + dexPath + " " +
                kernelClass.getName().replace('.', '/') + ".class";
        try {
            Process convertToDex = Runtime.getRuntime().exec(cmd, null, kernelClassDir);
            convertToDex.waitFor();
        } catch(IOException | InterruptedException e) {
            U.err(e.getMessage());
        }
        return Files.readAllBytes(Paths.get(dexPath));
    }
}