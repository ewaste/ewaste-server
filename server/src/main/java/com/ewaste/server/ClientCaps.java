package com.ewaste.server;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

class ClientCaps {

    private static int CPU_MAX = 64;
    private int cpus = 1;

    ClientCaps(int cpus) throws CapsParsingException, IOException {
        if(cpus < 1 || cpus > CPU_MAX)
            throw new CapsParsingException("CPUs must be between 1 and "+CPU_MAX);
        U.out(this.toString());
    }

    int getCpus() {
        return cpus;
    }
}


class CapsParsingException extends Exception {
     CapsParsingException(String cause) {
         super(cause);
     }
}