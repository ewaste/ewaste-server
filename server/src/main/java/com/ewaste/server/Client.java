package com.ewaste.server;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.Base64;


public class Client<InT, ResT> implements Runnable {

    private Thread clientThread;
    private Socket ownSocket;
    private OutputStream outputStream;
    private InputStream inputStream;
    private ClientCaps caps;
    private long id;
    private Server<InT, ResT> server;
    private int packCount;
    boolean done;

    Client(Socket s, long id, Server server) {
        try {
            done = false;
            outputStream = s.getOutputStream();
            inputStream = s.getInputStream();
            clientThread = new Thread(this);
            ownSocket = s;
            this.id = id;
            clientThread.start();
            this.server = server;
        } catch (IOException e) {
            U.err( e.getMessage() );
        }
    }

    private boolean accept = true;

    public void run() {
        if( greet() && sendKernel() ) {
            while (accept) {
                List<InT> inputAsInT = server.getNextData(packCount);
                if (inputAsInT != null) {
                    List<byte[]>  inputAsBytes = new ArrayList<>();
                    long timeStart = System.currentTimeMillis();
                    for(InT d : inputAsInT) {
                        inputAsBytes.add(server.callbacks.encodeInput(d));
                    }
                    List<byte[]> resultAsBytes = sendDataReceiveResult(inputAsBytes);
                    if (resultAsBytes == null) { //client failed to compute
                        server.returnData(inputAsInT);
                        stop();
                    } else {
                        List<ResT> resultAsResT = new ArrayList<>();
                        for(byte[] r : resultAsBytes) {
                            resultAsResT.add(server.callbacks.decodeResult(r));
                        }
                        server.appendResult(inputAsInT, resultAsResT);
                    }
                    long timeEnd = System.currentTimeMillis();
                    System.out.printf("%d, %d ms\n", packCount, timeEnd-timeStart);
                } else {
                    shutdown();
                }
            }
        } else {
            stop();
        }
    }

    void shutdown() {
        if(clientThread.isAlive()) {
            sayGoodbye();
            stop();
        }
    }

    private void stop() {
        accept = false;
        try {
            inputStream.close();
            outputStream.close();
            clientThread.join(1000);
            clientThread.interrupt();
        } catch (InterruptedException | IOException e) {
            U.err( e.getMessage() );
        }
    }

    private JSONObject receive(int waitLimit) throws TimeoutException, InterruptedException, IOException {
        int retry = 0;
        while(retry < waitLimit && inputStream.available() <= 0) {
            Thread.sleep(1000);
            retry++;
        }
        if(retry >= waitLimit) {
            throw new TimeoutException();
        } else {
            byte[] buffer = new byte[inputStream.available()];
            inputStream.read(buffer);
            return new JSONObject(new String(buffer));
        }
    }

    private void send(JSONObject obj) throws IOException{
        outputStream.write(obj.toString().getBytes());
        outputStream.flush();
    }

    private void sayGoodbye() {
        String baseError = String.format("sayGoodbye() failure for client %X: ", id);
        try {
            String stringReq = "{\"command\":\"bye\"}";
            send(new JSONObject(stringReq));
            JSONObject res = receive(36);
            if (!res.getString("message").equals("ack")) {
                U.err(baseError + "Client failed to shutdown.");
            }
            U.out(String.format("Successfully waved goodbye to client %X.", id));
        } catch (IOException | InterruptedException | TimeoutException e) {
            U.err( baseError + e.getMessage() );
        }
        done = true;
    }

    private ArrayList<byte[]> sendDataReceiveResult(List<byte[]> data) {
        String baseError = String.format("sendData() failure for client %X: ", id);
        try {
            JSONArray arrOut = new JSONArray();
            for(byte[] d: data) {
                arrOut.put(Base64.getEncoder().encodeToString(d));
            }
            String stringReq = "{\"command\":\"data\", \"data\":" + arrOut.toString() + "}";
            send(new JSONObject(stringReq));
            U.out(String.format("Sent data to client %X.", id));
            JSONObject res = receive(3600); // kernel execution time limit
            if (!res.getString("message").equals("result")) {
                U.err(baseError + "Client failed to compute kernel.");
                return null;
            }
            U.out(String.format("Received result from client %X.", id));
            JSONArray resArr = res.getJSONArray("result");
            ArrayList<byte[]> results = new ArrayList<byte[]>();
            for(int i=0;i<resArr.length();i++) {
                results.add(Base64.getDecoder().decode(resArr.getString(i).getBytes()));
            }
            return results;
        } catch (IOException | InterruptedException | TimeoutException e) {
            U.err( baseError + e.getMessage() );
            return null;
        }
    }

    private boolean sendKernel() {
        String baseError = String.format("sendKernel() failure for client %X: ", id);
        try {
            String stringReq = "{\"command\":\"kernel\", \"kernelClassName\":\"";
            stringReq += server.kernelClassName;
            stringReq += "\", \"kernelDex\":\"";
            stringReq += Base64.getEncoder().encodeToString(server.dexKernel);
            stringReq += "\"}";
            send(new JSONObject(stringReq));
            JSONObject res = receive(1000);
            if (!res.getString("message").equals("ack")) {
                U.err(baseError + "Client does not want kernel.");
                return false;
            }
        } catch (IOException | InterruptedException | TimeoutException e) {
            U.err( baseError + e.getMessage() );
            return false;
        }
        U.out(String.format("Sent kernel to client %X.", id));
        return true;
    }

    private boolean greet() {
        String baseError = String.format("greet() failure for client %X: ", id);
        try {
            String stringReq = String.format("{\"command\":\"greet\", \"id\":\"%d\"}", id);
            send(new JSONObject(stringReq));
            JSONObject res = receive(1000);
            if (!res.getString("message").equals("caps")) {
                U.err(baseError + "Client doesn't want to greet.");
                return false;
            }
            JSONObject capsJSON = res.getJSONObject("caps");
            caps = new ClientCaps(capsJSON.getInt("cores"));
            packCount = server.packCount*caps.getCpus();
            U.out(String.format("Sucessful handshake, new client with id %X", id ));
            return true;
        } catch (IOException | InterruptedException | TimeoutException | CapsParsingException e) {
            U.err( baseError + e.getMessage() );
            return false;
        }
    }

}
