package com.ewaste.server;

public interface IServerCallbacks<InT, ResT> {
    void gatherResult(InT out, ResT result);

    ResT decodeResult(byte[] resultBytes);

    byte[] encodeInput(InT input);
}
